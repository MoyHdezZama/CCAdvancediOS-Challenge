//
//  ImagesTableViewController.swift
//  CCAdvancediOS
//
//  Copyright © 2017 Wizeline Academy. All rights reserved.
//

import UIKit
import Foundation

class TableViewController: UITableViewController {
    
    var parser = ResponseParser()
    var networkingClient = NetworkClient()
    var dataSource = DataSourceManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 60

        guard let url = K.APIRoute.url else { return }
        
        networkingClient.fetch(url: url) { [weak weakSelf = self] (data, error) in
            weakSelf?.dataSource.source(model: weakSelf?.parser.parse(data: data))
            weakSelf?.tableView.reloadData()
        }
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.numberOfSections
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.numberOfRowsInSection(section: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: K.CellID.main, for: indexPath) as? TableViewCell,
                let dataModel = dataSource.modelAt(indexPath: indexPath) as? Weather else {
            return UITableViewCell()
        }
        
        cell.cityName.text = dataModel.city
        cell.weatherDescription.text = dataModel.desc
        
        return cell
    }
    
}
