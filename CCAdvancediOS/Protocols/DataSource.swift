//
//  DataSource.swift
//  CCAdvancediOS
//
//  Created by Moy Hdez on 20/07/17.
//  Copyright © 2017 Wizeline Academy. All rights reserved.
//

import Foundation

protocol DataSource {
    var numberOfSections: Int { get }
    
    func numberOfRowsInSection(section: Int) -> Int
    
    func modelAt(indexPath: IndexPath) -> DataModel
    
    mutating func source(model: [DataModel]?)
}

struct DataSourceManager: DataSource {
    
    var data: [DataModel] = []
    
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return data.count
    }
    
    func modelAt(indexPath: IndexPath) -> DataModel {
        return data[indexPath.row]
    }
    
    mutating func source(model: [DataModel]?) {
        guard let model = model else { return }
        self.data = model
    }
    
}
