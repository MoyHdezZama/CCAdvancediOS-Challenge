//
//  DataModel.swift
//  CCAdvancediOS
//
//  Created by Moy Hdez on 20/07/17.
//  Copyright © 2017 Wizeline Academy. All rights reserved.
//

import Foundation

protocol DataModel {}

struct Weather: DataModel {
    var city: String
    var desc: String
}
