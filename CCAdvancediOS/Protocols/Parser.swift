//
//  Parser.swift
//  CCAdvancediOS
//
//  Created by Moy Hdez on 20/07/17.
//  Copyright © 2017 Wizeline Academy. All rights reserved.
//

typealias JSON = [AnyHashable: Any]

protocol Parser {
    func parse(data: JSON?) -> [DataModel]?
}

struct ResponseParser: Parser {
    
    func parse(data: JSON?) -> [DataModel]? {
        guard let json = data, let cities = json["list"] as? [JSON] else { return nil }
        var array: [DataModel] = []
        for city in cities {
            let name = city["name"] as? String ?? ""
            if let weather = city["weather"] as? [JSON] {
                if let cityWeather = weather.first {
                    let desc = cityWeather["description"] as? String ?? ""
                    array.append(Weather(city: name, desc: desc))
                }
            }
        }
        return array
    }
}
