//
//  NetworkClient.swift
//  CCAdvancediOS
//
//  Created by Moy Hdez on 20/07/17.
//  Copyright © 2017 Wizeline Academy. All rights reserved.
//

import Foundation
import Alamofire

enum NetworkError: Error {
    case genericError
}

protocol Networking {
    func fetch(url: URL, completion: @escaping (JSON?, NetworkError?) -> Void)
}

struct NetworkClient: Networking {
    
    func fetch(url: URL, completion: @escaping (JSON?, NetworkError?) -> Void) {
        Alamofire.request(url).responseJSON { (response) in
            if response.result.isSuccess {
                guard let json = response.result.value as? JSON else { completion(nil, NetworkError.genericError); return }
                completion(json, nil)
            } else {
                completion(nil, NetworkError.genericError)
            }
        }
    }
}
