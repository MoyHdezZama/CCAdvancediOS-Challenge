//
//  TableViewCell.swift
//  CCAdvancediOS
//
//  Copyright © 2017 Wizeline Academy. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var weatherDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        cityName.translatesAutoresizingMaskIntoConstraints = false
        weatherDescription.translatesAutoresizingMaskIntoConstraints = false
        
        let views: [String: Any] = ["c": cityName, "w": weatherDescription]
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[c]-[w]-|", options: [], metrics: nil, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[w]-|", options: [], metrics: nil, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[c]-|", options: [], metrics: nil, views: views))
        
    }
}
