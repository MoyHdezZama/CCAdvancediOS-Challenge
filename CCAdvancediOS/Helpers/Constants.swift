//
//  Constants.swift
//  CCAdvancediOS
//
//  Created by Moy Hdez on 20/07/17.
//  Copyright © 2017 Wizeline Academy. All rights reserved.
//

import Foundation

class K {
    
    struct APIRoute {
        static let url = URL(string: "http://samples.openweathermap.org/data/2.5/box/city?bbox=12,32,15,37,10&appid=b1b15e88fa797225412429c1c50c122a1")
    }
    
    struct CellID {
        static let main = "cellidentifier"
    }
}
