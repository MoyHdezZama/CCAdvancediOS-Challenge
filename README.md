### INSTRUCTIONS:
The goal of this App is to display weather data from a JSON endpoint in a `UITableViewController`.

* Display only the city name and the weather description in the custom `TableViewCell`.
[Data](http://samples.openweathermap.org/data/2.5/box/city?bbox=12,32,15,37,10&appid=b1b15e88fa797225412429c1c50c122a1)

#### How the challenge result should look.
![simulator screen shot jun 27 2017 10 50 14 pm](https://user-images.githubusercontent.com/6756995/27621049-bf3a1ef6-5b93-11e7-9ae9-11d49bbe012d.png)

This challenge is planned to be Protocol Oriented Design. There are several suggested protocols your application can conform to. You can add, modify, or delete any Protocol_Structure_Class or line of code you want. Feel free to use any Pod or library

The application is crashing due to a UI bug. You have to correct it as well.
**Hint:**  The view hierarchy is not prepared for the constraint.

**Development Time Allotted:** ~1 hour.
